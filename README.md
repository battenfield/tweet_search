# Tweet Search

## To Run locally please follow these steps seed/connect mysql db (please use mysql version ^ 5.7)
* cd into repo


* login into mysql from cli ```mysql --local-infile=1 -u <your username> -p```


* create new db ```create database <database name>```  then use db  ```use <database name>```


* create new table called tweets in data format

```CREATE TABLE tweets (time DATETIME, username VARCHAR(250), text VARCHAR(250) );```



* load data into tweets table

```load data local infile 'time_username_tweets_tab.tsv' into table tweets fields terminated by '\t';```



* add your mysql login into dev.js to config folder

```
//dev.js file
module.exports = {
  host: <your host> (localhost),
  user: <your username> (usually root),
  password: <yourpassword>,
  database: <yourdatabase>
};
```

* ```npm run dep```

* ```npm run start```

## Dev Environment
    "node": "8.6.0",
    "npm": "5.0.3"

  MySQL version used 5.7.19


## Issues
> * Currently the clicking on # or @ is not opperational. Will look to add handler to api / sql query for this feature

> * Enhance Search results  

> * Will look to create an npm script to seed data
