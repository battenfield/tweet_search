import axios from 'axios';
import { FETCH_TWEETS } from './types';

export const fetchTweets = (search = "") => async dispatch => {
  const res = await axios.get('/api/tweets', { params: { search } });
  dispatch({ type: FETCH_TWEETS, payload: res.data });
};
