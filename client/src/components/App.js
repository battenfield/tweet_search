import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import Tweets from './Tweets';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { term: '' };
    this.onSearchChange = this.onSearchChange.bind(this);
  }
  
  componentWillMount() {
    this.fetchTweets();
  }

  onSearchChange(e) {
    this.setState({ term: e.target.value }, () => {
      this.fetchTweets();
    });
  }

  fetchTweets() {
    this.props.fetchTweets(this.state.term);
  }

  render() {
    return (
      <div className="container">
        <h1 className="nav-wrapper">Tweet Search</h1>
        <input className="" autoFocus onChange={e => this.onSearchChange(e)} />
        <Tweets tweets={this.props.tweets}/>
      </div>
    );
  }
}

function mapStateToProps({ tweets }) {
  return { tweets }
}



export default connect(mapStateToProps, actions)(App);