import React from 'react';
import _ from 'lodash';
import Linkify from 'react-linkify';

const Tweets = props => {
  const tweetResults = _.map(props.tweets, (tweet, id) => (
    <div className="row card-panel light-blue accent-1 z-depth-4 hoverable" key={id}>
      <Linkify className="col s10">{_.get(tweet, 'text')}</Linkify>
      <div className="col s2 btn blue">{_.get(tweet, 'username')}</div>
    </div>
  ));    
  return (
    <div>
      {tweetResults}
    </div>
  );
}

export default Tweets;