const express = require('express');
const app = express();
const mysql = require('mysql');
const dbLogin = require('./config/keys');

//api to recieve data
app.get('/api/tweets', (req, res) => {
  var connection = mysql.createConnection(dbLogin);
  connection.connect();
  const search =`%${req.query.search}%`;
  connection.query('SELECT text, username FROM tweets WHERE text LIKE ?', search, (err, results, fields) => {
    if(err) console.log('Connection error: ', err);
    const tweets = results;
    res.send(tweets);    
  });
  connection.end();
});

//serve up build for prod
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));

//catch all to server index file
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const PORT = process.env.PORT || 5000;
app.listen(PORT);
